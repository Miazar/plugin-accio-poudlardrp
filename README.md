# Accio plugin

This is a test Minecraft test plugin for Poudlard RP

The compiled `.jar` file can be found at `out/accio/accio.jar`

## Features

Add the accio spell that has a range of 50u and allow have different effects:
  
  - Remove grass / dirt block to give them to the player
  - Pull lever to activate them (A `repulso` could disable them for instance, if ever implemented)
  - Kill enemies and make creepers go away (that one was only to make the test easier)
  - When hitting a player, it gives the target's in-hand item to the player that casted the spell
  - Attract items on the ground to the player

It can only be used with "sticks". 

For convenience purpose a player get one when joining the game. The `wand` gives one too.