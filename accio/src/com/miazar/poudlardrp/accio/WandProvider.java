package com.miazar.poudlardrp.accio;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class WandProvider implements Listener, CommandExecutor {

    private void giveWandToPlayer(Player player){
        Inventory playerInventory = player.getInventory();
        ItemStack[] playerItems = playerInventory.getContents();
        for (int i = 0; i < playerItems.length; i++) {
            ItemStack item = playerInventory.getItem(i);
            if (item != null && item.getType() == Material.STICK) {
                // Player already have his wand
                return;
            }
        }

        playerInventory.addItem(new ItemStack(Material.STICK, 1));
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        giveWandToPlayer(event.getPlayer());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            return false;
        }

        giveWandToPlayer((Player) sender);
        return true;
    }

}
