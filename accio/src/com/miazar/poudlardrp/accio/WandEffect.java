package com.miazar.poudlardrp.accio;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Lever;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.util.Collection;

public class WandEffect implements Listener {

    @EventHandler
    public void onPlayerUse(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();
        World world = player.getWorld();

        if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
            if (player.getInventory().getItemInMainHand().getType() == Material.STICK) {
                BlockIterator i = new BlockIterator(player, 50);
                Particle.DustOptions dustOptions = new Particle.DustOptions(Color.fromRGB(0, 127, 255), 1);
                while (i.hasNext()) {
                    Block b = i.next();

                    Collection<Entity> entities = world.getNearbyEntities(b.getLocation(), 1, 1, 1);
                    for (Entity e : entities) {
                        switch (e.getType()) {
                            case PLAYER:
                                Player target = (Player)e;
                                if (target == player) {
                                    break;
                                }
                                ItemStack item = target.getInventory().getItemInMainHand();
                                player.getInventory().addItem(item);
                                target.getInventory().remove(item);
                                world.playSound(e.getLocation(), Sound.ITEM_CROSSBOW_SHOOT, 10, 1);
                                return;

                            case CREEPER:
                                e.remove();
                                world.playSound(e.getLocation(), Sound.ENTITY_CREEPER_HURT, 10, 1);
                                return;

                            default:
                                if (e instanceof Damageable) {
                                    ((Damageable) e).setHealth(0);
                                    e.setLastDamageCause(new EntityDamageEvent(e, EntityDamageEvent.DamageCause.MAGIC, 4242));
                                    world.playSound(e.getLocation(), Sound.ENTITY_EVOKER_CAST_SPELL, 10, 1);
                                    return;
                                }

                                // If it can take damages - just do a real accio and attract it to the player
                                e.setGravity(false);
                                Vector entityPos = e.getLocation().toVector();
                                Vector playerPos = player.getLocation().toVector();
                                Vector velocity = playerPos.subtract(entityPos);
                                e.setVelocity(velocity.normalize());
                                return;
                        }
                    }

                    if (b.getType() == Material.LEVER) {
                        // Pull a lever and enable it. Can never disable it
                        BlockState state = b.getState();
                        Lever lever = (Lever) state.getData();
                        lever.setPowered(true);
                        state.setData(lever);
                        state.update(true);
                        world.playSound(b.getLocation(), Sound.BLOCK_LEVER_CLICK, 10, 1);
                        return;
                    }

                    if (b.getType() == Material.DIRT || b.getType() == Material.GRASS_BLOCK) {
                        // Get the block
                        player.getInventory().addItem(new ItemStack(b.getType(), 1));
                        // We don't want this item to drop its loot
                        b.setType(Material.AIR);
                        world.playSound(b.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 10, 1);
                        return;
                    }


                    if (b.getType() != Material.AIR && b.getType() != Material.WATER) {
                        // The spell hit something
                        world.spawnParticle(Particle.CLOUD, b.getLocation(), 5);
                        return;
                    }

                    world.spawnParticle(Particle.REDSTONE, b.getLocation(), 10, dustOptions);
                }
            }
        }
    }
}
