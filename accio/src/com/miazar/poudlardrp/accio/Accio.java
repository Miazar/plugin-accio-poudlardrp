package com.miazar.poudlardrp.accio;

import org.bukkit.plugin.java.JavaPlugin;

public class Accio extends JavaPlugin {
    @Override
    public void onEnable(){
        WandProvider wp = new WandProvider();
        getCommand("wand").setExecutor(wp);
        getServer().getPluginManager().registerEvents(wp, this);
        getServer().getPluginManager().registerEvents(new WandEffect(), this);
    }

    @Override
    public void onDisable(){

    }
}
